/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hrsbackend.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Akarshan
 */
@Entity
@Table(name = "tablelist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tablelist.findAll", query = "SELECT t FROM Tablelist t"),
    @NamedQuery(name = "Tablelist.findByTableid", query = "SELECT t FROM Tablelist t WHERE t.tableid = :tableid"),
    @NamedQuery(name = "Tablelist.findByConfcode", query = "SELECT t FROM Tablelist t WHERE t.confcode = :confcode"),
    @NamedQuery(name = "Tablelist.findBySize", query = "SELECT t FROM Tablelist t WHERE t.size = :size"),
    @NamedQuery(name = "Tablelist.findByStatus", query = "SELECT t FROM Tablelist t WHERE t.status = :status"),
    @NamedQuery(name = "Tablelist.findBySince", query = "SELECT t FROM Tablelist t WHERE t.since = :since")})
public class Tablelist implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tableid")
    private Integer tableid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "confcode")
    private String confcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "size")
    private String size;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "since")
    private String since;

    public Tablelist() {
    }

    public Tablelist(Integer tableid) {
        this.tableid = tableid;
    }

    public Tablelist(Integer tableid, String confcode, String size, String status, String since) {
        this.tableid = tableid;
        this.confcode = confcode;
        this.size = size;
        this.status = status;
        this.since = since;
    }

    public Integer getTableid() {
        return tableid;
    }

    public void setTableid(Integer tableid) {
        this.tableid = tableid;
    }

    public String getConfcode() {
        return confcode;
    }

    public void setConfcode(String confcode) {
        this.confcode = confcode;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tableid != null ? tableid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tablelist)) {
            return false;
        }
        Tablelist other = (Tablelist) object;
        if ((this.tableid == null && other.tableid != null) || (this.tableid != null && !this.tableid.equals(other.tableid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.hrsbackend.model.Tablelist[ tableid=" + tableid + " ]";
    }
    
}
